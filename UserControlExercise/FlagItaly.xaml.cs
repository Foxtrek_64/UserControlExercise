﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace UserControlExercise
{
    /// <summary>
    /// Interaction logic for FlagItaly.xaml
    /// </summary>
    public partial class FlagItaly : UserControl
    {
        private string _header = "Italy";
        public string Header
        {
            get => _header;
            set
            {
                if (value != _header)
                {
                    _header = value;
                    OnPropertyChanged("Header");
                }
            }
        }

        private string _contents = "The flag...";
        public string Contents
        {
            get => _contents;
            set
            {
                if (value != _contents)
                {
                    _contents = value;
                    OnPropertyChanged("Contents");
                }
            }
        }

        public FlagItaly()
        {
            InitializeComponent();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
